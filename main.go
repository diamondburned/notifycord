package main

import (
	"log"
	"os"
	"time"

	"github.com/urfave/cli"
)

var (
	args = struct {
		Format        string
		DMFormat      string
		Timeout       float64
		IgnoreBlocked bool
		MaxLen        int
		JSON          bool
		TimeFmt       string
	}{}

	exit = make(chan struct{})
)

func main() {
	log.SetFlags(log.Lshortfile)

	app := cli.NewApp()
	app.Name = "notifycord"
	app.Usage = "Discord notifier, made for lemonbar"
	app.ArgsUsage = "token"
	app.EnableBashCompletion = true

	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:        "l, maxlen",
			Usage:       "Sets the maximum body length, -1 is unlimited",
			Value:       -1,
			Destination: &args.MaxLen,
		},
		cli.BoolFlag{
			Name:        "j, json",
			Usage:       "Dump JSON output instead, ignores timeout flag",
			Destination: &args.JSON,
		},
		cli.BoolFlag{
			Name:        "i, ignoreblocked",
			Usage:       "Ignores blocked users",
			Destination: &args.IgnoreBlocked,
		},
		cli.Float64Flag{
			Name:        "t, timeout",
			Usage:       "Sets the timeout before disappearing in seconds, -1 for permanent",
			Value:       5,
			Destination: &args.Timeout,
		},
		cli.StringFlag{
			Name:        "f, format",
			Usage:       "Formatting template {{string}}, refer to ./notifycord template",
			Value:       "#{{channel}} {{user}}: {{content}}",
			Destination: &args.Format,
		},
		cli.StringFlag{
			Name:        "dmf, dmformat",
			Usage:       "Formatting template {{string}} for direct messages, refer to ./notifycord template",
			Value:       "#{{channel}} {{user}}#{{discrim}}: {{content}}",
			Destination: &args.Format,
		},
		cli.StringFlag{
			Name:        "tfmt, timefmt",
			Usage:       "Time format, refer to https://godate.diamondb.xyz",
			Value:       time.Kitchen,
			Destination: &args.TimeFmt,
		},
	}

	app.Commands = []cli.Command{
		cli.Command{
			Name:  "template",
			Usage: "Shows a manual page for template strings",
			Action: func(c *cli.Context) error {
				print(
					`All templating variables:
    - guild   - guild name, empty in DM
    - channel - channel name
    - user    - author's username, nickname if available
    - discrim - author's discriminator
    - content - the message content
    - time    - the timestamp, configurable with --timefmt
    
Example utilizing all 5 variables:
    {{ guild }} - #{{ channel }} - @{{ user }} - {{ time }} - {{ content }}
    
Expected output:
    r/unixporn - #home - @ym555 - 3:03PM - Test message content @_diamondburned_
    
Behavioral notices:
    - Direct Message format is used when the incoming message is from a direct message channel with no name
    - Group direct messages with a name will use the normal format
    - DM format, when empty, will use the normal format
`,
				)

				return nil
			},
		},
	}

	cli.OsExiter = func(c int) {
		exit <- struct{}{}
	}

	app.Action = application

	if err := app.Run(os.Args); err != nil {
		println(err.Error())
	}
}
