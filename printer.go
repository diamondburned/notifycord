package main

import (
	"fmt"
	"os"
	"time"
)

// boschetar Today at 23:01
// https://play.golang.org/p/9JgKay_2Vqm

type messagePrinter struct {
	msgs chan string
	d    time.Duration
	t    *time.Timer
}

func newMessagePrinter(strobe time.Duration) *messagePrinter {
	mp := &messagePrinter{
		msgs: make(chan string),
		d:    strobe,
		t:    time.NewTimer(strobe),
	}

	go mp.loop()
	return mp
}

func (mp *messagePrinter) loop() {
	fired := false
	for {
		select {
		case <-mp.t.C:
			fmt.Fprintln(os.Stdout, "")
			fired = true

		case msg, ok := <-mp.msgs:
			if !ok {
				return
			}

			fmt.Fprint(os.Stdout, "\n"+msg)

			if !mp.t.Stop() && !fired {
				<-mp.t.C
			}

			mp.t.Reset(mp.d)
			fired = false
		}
	}
}

func (mp *messagePrinter) Print(msg string) {
	mp.msgs <- msg
}
