package main

import "github.com/bwmarrin/discordgo"

const (
	RelationshipNone int = iota

	RelationshipFriend                // friend
	RelationshipBlocked               // blocked
	RelationshipIncomingFriendRequest // incoming friend request
	RelationshipSentFriendRequest     // sent friend request
)

func userSettingsUpdate(s *discordgo.Session, settings *discordgo.UserSettingsUpdate) {
	if settings == nil {
		return
	}

	_settings := *settings

	if status, ok := _settings["status"]; ok {
		if str, ok := status.(string); ok {
			st := discordgo.Status(str)
			s.State.Settings.Status = st
		}
	}
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if args.IgnoreBlocked {
		// Ignore the message if it's from a blocked user
		for _, r := range s.State.Relationships {
			if r.User.ID == m.Author.ID && r.Type == RelationshipBlocked {
				return
			}
		}
	}

	// Don't send if the status is Busy/Do not Disturb
	if s.State.Settings != nil && s.State.Settings.Status == discordgo.StatusDoNotDisturb {
		return
	}

	if c, _ := s.State.Channel(m.ChannelID); c != nil {
		for _, gs := range s.State.UserGuildSettings {
			if gs.GuildID == c.GuildID {
				if !gs.SupressEveryone && m.MentionEveryone {
					goto Notify
				}

				if gs.GuildID != "" && gs.Muted {
					return
				}

				for _, cs := range gs.ChannelOverrides {
					if cs.ChannelID == c.ID {
						if cs.Muted {
							return
						}
					}
				}
			}
		}

		if c.GuildID == "" {
			if m.Author.ID != s.State.User.ID {
				goto Notify
			}

			return
		}
	}

	for _, p := range m.Mentions {
		if p.ID == s.State.User.ID {
			goto Notify
		}
	}

	return

Notify:
	notifyMessage(s, m.Message)
}
