package main

import (
	"encoding/json"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/valyala/fasttemplate"
)

var (
	fmtTpl, dmTpl *fasttemplate.Template

	// the printer buffer to time when to clear the line
	pr *messagePrinter
)

func notifyMessage(s *discordgo.Session, m *discordgo.Message) {
	var (
		isDM = true
		kv   = map[string]interface{}{
			"guild":   "",
			"channel": "",
			"user":    m.Author.Username,
			"discrim": m.Author.Discriminator,
			"content": m.ContentWithMentionsReplaced(),
			"time":    "",
		}
	)

	if c, err := m.ContentWithMoreMentionsReplaced(s); err != nil {
		kv["content"] = c
	}

	{
		// kv["content"] is pretty much guaranteed to be a string
		// no type-checks needed
		c := kv["content"].(string)

		// Trim to args.MaxLen
		if len(c) > args.MaxLen && args.MaxLen >= 3 {
			c = c[:args.MaxLen-3] + "..."
		}

		if len(m.Embeds) > 0 {
			c += " [embed]"
		}

		if len(m.Attachments) > 0 {
			c += " [attachments]"
		}

		kv["content"] = c
	}

	if t, err := m.Timestamp.Parse(); err == nil {
		kv["time"] = t.Format(args.TimeFmt)
	}

	if c, _ := s.State.Channel(m.ChannelID); c != nil {
		switch {
		case c.Name != "":
			kv["channel"] = c.Name
		case len(c.Recipients) > 0:
			switch len(c.Recipients) {
			case 0:
			case 1:
				kv["channel"] = c.Recipients[0].Username + "#" + c.Recipients[0].Discriminator
			case 2:
				kv["channel"] = c.Recipients[0].Username + " and " + c.Recipients[1].Username
			default:
				// strings.Builder for optimization
				var s strings.Builder

				for i := 0; i < len(c.Recipients)-2; i++ {
					s.WriteString(c.Recipients[i].Username + ", ")
				}

				s.WriteString(c.Recipients[len(c.Recipients)-2].Username)
				s.WriteString(" and ")
				s.WriteString(c.Recipients[len(c.Recipients)-1].Username)

				kv["channel"] = s.String()
			}
		}

		if g, _ := s.State.Guild(c.GuildID); g != nil {
			kv["guild"] = g.Name
			isDM = false

			if m, _ := s.State.Member(g.ID, m.Author.ID); m != nil {
				if m.Nick != "" {
					kv["user"] = m.Nick
				}
			}
		}
	}

	if !args.JSON {
		if isDM {
			pr.Print(dmTpl.ExecuteString(kv))
		} else {
			pr.Print(fmtTpl.ExecuteString(kv))
		}
	} else {
		if j, err := json.Marshal(kv); err == nil {
			pr.Print(string(j))
		} else {
			pr.Print(err.Error())
		}
	}
}
