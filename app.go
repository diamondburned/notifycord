package main

import (
	"errors"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/urfave/cli"
	"github.com/valyala/fasttemplate"
)

var (
	errorMissingToken = errors.New("Missing token for $1")
)

func application(c *cli.Context) (err error) {
	token := c.Args().First()
	if token == "" {
		return errorMissingToken
	}

	fmtTpl, err = fasttemplate.NewTemplate(args.Format, "{{", "}}")
	if err != nil {
		return
	}

	if !args.JSON {
		if args.DMFormat != "" {
			dmTpl, err = fasttemplate.NewTemplate(args.DMFormat, "{{", "}}")
			if err != nil {
				return
			}
		} else {
			dmTpl = fmtTpl
		}
	}

	pr = newMessagePrinter(time.Duration(
		args.Timeout * float64(time.Second),
	))

	pr.Print("Initializing...")

	session, err := discordgo.New(token)
	if err != nil {
		return err
	}

	session.State.TrackChannels = true
	session.State.TrackMembers = true

	session.AddHandler(userSettingsUpdate)
	session.AddHandler(messageCreate)
	session.AddHandlerOnce(func(s *discordgo.Session, r *discordgo.Ready) {
		pr.Print("Discord is ready.")
	})

	if err := session.Open(); err != nil {
		return err
	}

	defer session.Close()

	<-exit

	return nil
}
