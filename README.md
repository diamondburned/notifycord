# notifycord

A Discord daemon to print notifications, made per request of some people on the r/unixporn Discord

![](https://media.discordapp.net/attachments/361912021886697473/561024742778208276/unknown.png)

# Usage

### Example

Lemonbar output:

```sh
./notifycord -f '%{F#FFFFFF}%{B#7289DA} #{{channel}} - {{user}} %{B#1D1D1D} {{content}}' $TOKEN | lemonbar -g 1600x12+0+$((900-12)) -B '#FF000000' -F '#FFFFFFFF' -f 'lucy tewi:size=9'<Paste>
```

JSON output:

```sh
./notifycord -j $TOKEN | while read -d $'\n' -r json; do
	jq '.channel' <<< "$json"
done
```

Help:

```sh
./notifycord help
```

