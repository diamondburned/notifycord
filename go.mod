module gitlab.com/diamondburned/notifycord

go 1.12

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/urfave/cli v1.20.0
	github.com/valyala/fasttemplate v1.0.1
)
